<?php

namespace Jyrmo\Gateway;

use Jyrmo\Router\RouterInterface;

class Gateway implements GatewayInterface {
	/**
	 * @var RequestBuilderInterface
	 */
	protected $requestBuilder;

	/**
	 * @var RouterInterface
	 */
	protected $router;

	/**
	 * @var ResponsePresenterInterface
	 */
	protected $responsePresenter;

	public function setRequestBuilder(RequestBuilderInterface $requestBuilder) {
		$this->requestBuilder = $requestBuilder;
	}

	public function getRequestBuilder() : RequestBuilderInterface {
		return $this->requestBuilder;
	}

	public function setRouter(RouterInterface $router) {
		$this->router = $router;
	}

	public function getRouter() : RouterInterface {
		return $this->router;
	}

	public function setResponsePresenter(ResponsePresenterInterface $responsePresenter) {
		$this->responsePresenter = $responsePresenter;
	}

	public function getResponsePresenter() : ResponsePresenterInterface {
		return $this->responsePresenter;
	}

	public function processRequest() {
		$request = $this->requestBuilder->build();
		try {
			$response = $this->router->route($request);
			$this->responsePresenter->present($response);
		} catch (\Exception $ex) {
			$this->responsePresenter->presentException($ex);
		}
	}
}
