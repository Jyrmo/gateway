<?php

namespace Jyrmo\Gateway;

interface GatewayInterface {
	public function processRequest();
}
