<?php

namespace Jyrmo\Gateway;

use Jyrmo\Router\RequestInterface;

interface RequestBuilderInterface {
	public function build() : RequestInterface;
}
