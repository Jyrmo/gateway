<?php

namespace Jyrmo\Gateway;

use Jyrmo\Router\ResponseInterface;

interface ResponsePresenterInterface {
	public function present(ResponseInterface $response);

	public function presentException(\Exception $ex);
}
